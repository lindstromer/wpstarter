# WP Starter

Vagrant machine with Ubuntu Server 12.10 with Ansible inside.

This setup serves as a development template starter with a plain WordPress installation.

Starter theme is [Roots Starter Theme](http://roots.io/) with [Grunt](http://gruntjs.com/) features.

![roots screenshot](https://bytebucket.org/flowcom/wpstarter/raw/master/assets/img/wpstarter-roots-cli.png "wpstarter screenshot")

## Requirements
1. VirtualBox
2. Vagrant
3. Git

*update* Ansible is runned inside the local machine and doesn't require Ansible installed on your computer.

## VirtualBox 4.3.10, startup issue APRIL 2014
Please add workaround described in Vagrant issues: [https://github.com/dotless-de/vagrant-vbguest/issues/117](https://github.com/dotless-de/vagrant-vbguest/issues/117)

`vagrant ssh`

`sudo ln -s /opt/VBoxGuestAdditions-4.3.10/lib/VBoxGuestAdditions /usr/lib/VBoxGuestAdditions`

`exit`

`vagrant reload`

## How to start - Step By Step
+ Install VirtualBox, Vagrant and Git!

+ Start your terminal

+ Clone your own vagrant folder

`git clone https://bitbucket.org/flowcom/wpstarter`

+ Go to the cloned folder

`cd wpstarter`

+ Ensure you have hostupdater plugin

`vagrant plugin install vagrant-hostsupdater`

+ Now, run Vagrant

`vagrant up`

*note* if you have installed Ansible locally set environment ANSIBLE before calling vagrant up, eg:

`ANSIBLE=true vagrant up`

+ Wait for the setup to create your Vagrant guest!

+ Go to the created web site **[http://wpstarter.dev](http://wpstarter.dev)**

+ Login to [wp-admin](http://wpstarter.dev/wp-admin) with admin/admin

*Have fun!*

## Troubleshooting
Vagrant is not that stable. Sometimes it gives error at startup.
You can try to restart the process, eg:

`vagrant reload --provision`

## Parameters
Common parameters is set in ansible/group_vars/localhost.yml, eg language settings for WordPress:

`wp_lang: en_US`

Change this one to get another WP-localization, eg for Swedish:

`wp_lang: sv_SE`


## Techniques
1. VirtualBox
1. Vagrant
1. Ansible (instead of Puppet :-)
1. WP-CLI


## Convert this project to your own boilerplate for WordPress development
1. Change all 'wpstarter' to your new host-name in the Vagrantfile. It's safe to to a search-replace in the whole folder.
1. Perhaps another ip-address in the Vagrantfile?
1. Change the parameters in ansible/group_vars/vagrant
1. Change the inventory host name, ansible/development

## Roadmap
We will continue attach modules to the setup for development support, eg grunt.

