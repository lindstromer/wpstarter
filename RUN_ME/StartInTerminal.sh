#!/bin/bash
cd ..

echo "*** checking for vagrant plugins installed ***"
installed=$(vagrant plugin list)

plugin="vagrant-login"
if [[ "$installed" =~ "$plugin" ]]; then
	echo "vagrant-login installed, ok!"
else
  vagrant plugin install vagrant-login
fi

plugin="vagrant-share"
if [[ "$installed" =~ "$plugin" ]]; then
	echo "vagrant-share installed, ok!"
else
  vagrant plugin install vagrant-share
fi

plugin="vagrant-vbguest"
if [[ "$installed" =~ "$plugin" ]]; then
	echo "vagrant-vbguest installed, ok!"
else
  vagrant plugin install vagrant-vbguest
fi

plugin="vagrant-hostsupdater"
if [[ "$installed" =~ "$plugin" ]]; then
	echo "vagrant-hostsupdater installed, ok!"
else
  vagrant plugin install vagrant-hostsupdater
fi

echo "*** checking for precise64 box ***"
installed=$(vagrant box list)

box="precise64"
if [[ "$installed" =~ "$box" ]]; then
	echo "precise64 installed, ok!"
else
  vagrant box add precise64 http://files.vagrantup.com/precise64.box
fi

vagrant up