<?php
/*
Plugin Name: WP-Starter Provision Plugin
Version: 0.0.12
Description: Experimental
*/

if ( $_REQUEST['key'] == '{{ provision_key }}' ) {

	$log = false;
	if ( isset( $_REQUEST['log'] ) ) {
		$log = true;
	}

	function execute_code( $code, $log=false ) {
		if( $log ) {
			echo "<p>Executing: " . $code . '<br/>';
			echo "Result = " . shell_exec( $code . ' 2>&1; echo $?' ) . '</p>';
		}
		else {
			shell_exec( $code );
		}
	}

	$temp_dir = realpath($_SERVER["DOCUMENT_ROOT"]) . '/wp-content/uploads';

	execute_code( 'rm -f ' . $temp_dir . '/wp-starter.tar.gz', $log );
	execute_code( 'rm -f ' . $temp_dir . '/backup.sql', $log );
	execute_code( 'rm -f ' . realpath($_SERVER["DOCUMENT_ROOT"]) . '/backup.sql', $log );

	execute_code( 'tar -cf ' . $temp_dir . '/backup.tar --exclude="wp-starter.tar.gz" --exclude="*.zip" -C ' . realpath($_SERVER["DOCUMENT_ROOT"]) . ' .', $log );

	if( $log ) {
		echo '<p>Executing: mysqldump<br/>';
	}

	$db = array();
	exec( 'mysqldump -u external -pexternal wp', $result );
	file_put_contents( $temp_dir . '/backup.sql', implode( "\n", $result ) );
	if( $log ) {
		echo 'Size:' . filesize( $temp_dir . '/backup.sql' ) . '</p>';
	}

	execute_code( 'tar --append --file=' . $temp_dir . '/backup.tar -C ' . $temp_dir . '/ backup.sql', $log );
	execute_code( 'rm -f ' . $temp_dir . '/backup.sql', $log );
	execute_code( 'gzip -f3 ' . $temp_dir . '/backup.tar', $log );
	execute_code( 'mv ' . $temp_dir . '/backup.tar.gz ' . $temp_dir . '/wp-starter.tar.gz', $log );

	if( !$log ) {
		session_start();
		header('Content-Description: File Transfer');
		header('Connection: Keep-Alive');
		header('Expires: 0');
		header("Pragma: public");
		header("Expires: 0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment; filename='wp-starter.tar.gz'");
		header("Content-Transfer-Encoding: binary ");
		header('Content-Length: ' . filesize( $temp_dir . '/wp-starter.tar.gz' ) );
		header('Pragma: no-cache');
		header('Cache-Control: private, no-cache, no-store, max-age=0, must-revalidate, proxy-revalidate');
		ob_clean();
		flush();
		readfile( $temp_dir . '/wp-starter.tar.gz' );
	}

	shell_exec( 'rm -f ' . $temp_dir . '/wp-starter.tar.gz' );
	exit;

}