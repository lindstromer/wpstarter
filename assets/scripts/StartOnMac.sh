#!/bin/sh 
osascript <<END 
tell application "Finder" to get folder of (path to me) as Unicode text
set workingDir to POSIX path of result
tell application "Terminal"
    set waiting_command to do script "cd '" & workingDir & "' && ./StartInTerminal.sh"
    repeat
        delay 1
        if not busy of waiting_command then exit repeat
    end repeat
end tell
END